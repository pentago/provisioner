#!/bin/bash
set -euo pipefail

# Setup regular user
USERNAME=user

# Whether to copy over the root user's `authorized_keys` file to the new sudo user
COPY_AUTHORIZED_KEYS_FROM_ROOT=true

# Additional public keys to add to the new sudo user
# OTHER_PUBLIC_KEYS_TO_ADD=(
#   "ssh-rsa AAAAB..."
#   "ssh-rsa AAAAB..."
# )
OTHER_PUBLIC_KEYS_TO_ADD=(
)

# Setup packages
echo "deb [arch=amd64] https://nginx.org/packages/mainline/ubuntu $(lsb_release -cs) nginx" >> /etc/apt/sources.list.d/nginx.list
echo "deb-src http://nginx.org/packages/mainline/ubuntu $(lsb_release -cs) nginx" >> /etc/apt/sources.list.d/nginx.list
curl -fsSL https://nginx.org/keys/nginx_signing.key | apt-key add -
export DEBIAN_FRONTEND=noninteractive
apt-get update -qq && apt-get full-upgrade -qqy && apt-get purge -qqy ufw snapd
apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -qq -y zsh tmux vim ranger htop curl git gnupg2 apt-transport-https ca-certificates  software-properties-common iptables-persistent ipset-persistent nginx sshfs exim4 bsd-mailx borgbackup dpkg-dev unzip build-essential zlib1g-dev libpcre3 libpcre3-dev uuid-dev libssl-dev

# Setup ngx_pagespeed module
mkdir /usr/local/src/nginx && cd /usr/local/src/nginx
apt source nginx && cd ..
git clone https://github.com/apache/incubator-pagespeed-ngx.git && cd incubator-pagespeed-ngx && git checkout latest-stable
curl -LO https://dl.google.com/dl/page-speed/psol/1.13.35.2-x64.tar.gz && tar xvf 1.13.35.2-x64.tar.gz
cd ../nginx/nginx-1.* && apt build-dep -y nginx
./configure --with-compat --add-dynamic-module=/usr/local/src/incubator-pagespeed-ngx && make modules && cp objs/ngx_pagespeed.so /etc/nginx/modules/
apt-get autoremove -qqy && apt-get clean && rm -rf /root/snap

# Add sudo user and grant privileges
useradd -m -s "/usr/bin/zsh" -G sudo "${USERNAME}"

# Check whether the root account has a real password set
encrypted_root_pw="$(grep root /etc/shadow | cut --delimiter=: --fields=2)"

if [ "${encrypted_root_pw}" != "*" ]; then
    # Transfer auto-generated root password to user if present
    # and lock the root account to password-based access
    echo "${USERNAME}:${encrypted_root_pw}" | chpasswd --encrypted
    passwd --lock root
else
    # Delete invalid password for user if using keys so that a new password
    # can be set without providing a previous value
    passwd --delete "${USERNAME}"
fi

# Expire the sudo user's password immediately to force a change
chage --lastday 0 "${USERNAME}"

# Create SSH directory for sudo user
home_directory="$(eval echo ~${USERNAME})"
mkdir --parents "${home_directory}/.ssh"

# Copy `authorized_keys` file from root if requested
if [ "${COPY_AUTHORIZED_KEYS_FROM_ROOT}" = true ]; then
    cp /root/.ssh/authorized_keys "${home_directory}/.ssh"
fi

# Add additional provided public keys
for pub_key in "${OTHER_PUBLIC_KEYS_TO_ADD[@]}"; do
    echo "${pub_key}" >>"${home_directory}/.ssh/authorized_keys"
done

# Adjust SSH Configuration Permissions
chmod 0700 "${home_directory}"
chmod 0700 "${home_directory}/.ssh"
chmod 0600 "${home_directory}/.ssh/authorized_keys"
chown -R "${USERNAME}":"${USERNAME}" "${home_directory}"

# Setup Logging
sed --in-place "s/^#cron/cron/g" /etc/rsyslog.d/50-default.conf
mkdir -p /var/log/iptables/old && chown -R syslog:adm /var/log/iptables
echo ':msg,contains,"PASS:" /var/log/iptables/pass.log' >>/etc/rsyslog.d/10-iptables.conf
echo ':msg,contains,"PING:" /var/log/iptables/ping.log' >>/etc/rsyslog.d/10-iptables.conf
echo ':msg,contains,"INVALID" /var/log/iptables/invalid.log' >>/etc/rsyslog.d/10-iptables.conf
echo ':msg,contains,"BLOCK:" /var/log/iptables/block.log' >>/etc/rsyslog.d/10-iptables.conf
echo '/var/log/iptables/*.log' >>/etc/logrotate.d/iptables
echo '{' >>/etc/logrotate.d/iptables
echo 'hourly' >>/etc/logrotate.d/iptables
echo 'rotate 720' >>/etc/logrotate.d/iptables
echo 'missingok' >>/etc/logrotate.d/iptables
echo 'notifempty' >>/etc/logrotate.d/iptables
echo 'compress' >>/etc/logrotate.d/iptables
echo 'delaycompress' >>/etc/logrotate.d/iptables
echo 'olddir /var/log/iptables/old' >>/etc/logrotate.d/iptables
echo '}' >>/etc/logrotate.d/iptables
logrotate -f /etc/logrotate.conf

# Setup SSH
SSH_PORT=44344
sed --in-place "s/^#Port 22/Port $SSH_PORT/g" /etc/ssh/sshd_config
sed --in-place "s/^#AddressFamily.*/AddressFamily inet/g" /etc/ssh/sshd_config
sed --in-place "s/^#HostKey/HostKey/g" /etc/ssh/sshd_config
sed --in-place "s/^#LoginGraceTime.*/LoginGraceTime 1m/g" /etc/ssh/sshd_config
sed --in-place "s/^#StrictModes.*/StrictModes yes/g" /etc/ssh/sshd_config
sed --in-place "s/^#PubkeyAuthentication.*/PubkeyAuthentication yes/g" /etc/ssh/sshd_config
sed --in-place "s/^X11Forwarding.*/X11Forwarding no/g" /etc/ssh/sshd_config
sed --in-place "s/^PermitRootLogin.*/PermitRootLogin prohibit-password/g" /etc/ssh/sshd_config

# Setup Time
ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime
sed --in-place "s/^#NTP=/NTP=time.cloudflare.com/g" /etc/systemd/timesyncd.conf
sed --in-place "s/^#FallbackNTP.*/FallbackNTP=europe.pool.ntp.org/g" /etc/systemd/timesyncd.conf

# Setup Nameservers
sed --in-place "s/^#DNS=/DNS=1.1.1.1 1.0.0.1/g" /etc/systemd/resolved.conf
sed --in-place "s/^#FallbackDNS=/FallbackDNS=8.8.8.8 8.8.4.4/g" /etc/systemd/resolved.conf
sed --in-place "s/^#Domains=/Domains=google.com/g" /etc/systemd/resolved.conf
sed --in-place "s/^#Cache/Cache/g" /etc/systemd/resolved.conf

# Setup Firewall
ipset create BLOCKED hash:ip family inet timeout 900
iptables -F
iptables -X
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT
iptables -N LOGDROP
iptables -N LOGINVALID
iptables -N LOGPASS
iptables -N LOGPING
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -m conntrack --ctstate INVALID -j LOGINVALID
iptables -A INPUT -m set --match-set BLOCKED src -j LOGDROP
iptables -A INPUT -p icmp -m conntrack --ctstate NEW -m icmp --icmp-type 8 -j LOGPING
iptables -A INPUT -p tcp -m tcp --dport "$SSH_PORT" -m state --state NEW -m recent --set --name DEFAULT --mask 255.255.255.255 --rsource
iptables -A INPUT -p tcp -m tcp --dport "$SSH_PORT" -m state --state NEW -m recent --update --seconds 60 --hitcount 6 --name DEFAULT --mask 255.255.255.255 --rsource -j SET --add-set BLOCKED src
iptables -A INPUT -p tcp -m tcp --dport "$SSH_PORT" -j ACCEPT
iptables -A INPUT -p tcp -m multiport --dports 80,443 -j ACCEPT
iptables -A INPUT -j LOGDROP
iptables -A LOGDROP -j LOG --log-prefix "BLOCK: "
iptables -A LOGDROP -j DROP
iptables -A LOGINVALID -j LOG --log-prefix "INVALID: "
iptables -A LOGINVALID -j DROP
iptables -A LOGPASS -j LOG --log-prefix "PASS: "
iptables -A LOGPASS -j ACCEPT
iptables -A LOGPING -j LOG --log-prefix "PING: "
iptables -A LOGPING -j ACCEPT
iptables-save >/etc/iptables/rules.v4
netfilter-persistent start && netfilter-persistent save

# Setup Services
systemctl disable unattended-upgrades
systemctl disable rsync
systemctl restart systemd-timesyncd
systemctl enable netfilter-persistent
systemctl enable nginx && systemctl start nginx
systemctl restart rsyslog
systemctl restart sshd

# Setup Swap
SWAP="1G"
fallocate -l $SWAP /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile none  swap  sw  0 0" >>/etc/fstab

# Reboot
reboot
